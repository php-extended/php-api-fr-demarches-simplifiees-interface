<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-demarches-simplifiees-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDemarchesSimplifiees;

use DateTimeInterface;
use Stringable;

/**
 * ApiFrDemarchesSimplifieesDossierItemInterface interface file.
 * 
 * This represents a basic informations for the dossiers.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiFrDemarchesSimplifieesDossierItemInterface extends Stringable
{
	
	/**
	 * Gets the id of the dossier.
	 * 
	 * @return int
	 */
	public function getId() : int;
	
	/**
	 * Gets the date at which the dossier was last updated.
	 * 
	 * @return DateTimeInterface
	 */
	public function getUpdatedAt() : DateTimeInterface;
	
	/**
	 * Gets the date at which the dossier was created.
	 * 
	 * @return DateTimeInterface
	 */
	public function getInitiatedAt() : DateTimeInterface;
	
	/**
	 * Gets the state of the dossier.
	 * 
	 * @return string
	 */
	public function getState() : string;
	
}
