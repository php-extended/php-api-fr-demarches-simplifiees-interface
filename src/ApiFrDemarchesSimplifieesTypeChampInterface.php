<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-demarches-simplifiees-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDemarchesSimplifiees;

use Stringable;

/**
 * ApiFrDemarchesSimplifieesTypeChampInterface interface file.
 * 
 * This represents a type of field.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiFrDemarchesSimplifieesTypeChampInterface extends Stringable
{
	
	/**
	 * Gets the id of the type of field.
	 * 
	 * @return int
	 */
	public function getId() : int;
	
	/**
	 * Gets the id of the related procedure.
	 * 
	 * @return int
	 */
	public function getProcedureId() : int;
	
	/**
	 * Gets the libelle of the type of field.
	 * 
	 * @return string
	 */
	public function getLibelle() : string;
	
	/**
	 * Gets the type of the type of field. One of "text", "email", "phone",
	 * "address", "number", "date", "drop_down_list" ...
	 * 
	 * @return string
	 */
	public function getType() : string;
	
	/**
	 * Gets the place of the type of field. Indexed by 0.
	 * 
	 * @return int
	 */
	public function getOrderPlace() : int;
	
	/**
	 * Gets  The description of the type of field.
	 * 
	 * @return string
	 */
	public function getDescription() : string;
	
	/**
	 * Gets whether this champ is mandatory or not.
	 * 
	 * @return bool
	 */
	public function hasMandatory() : bool;
	
}
