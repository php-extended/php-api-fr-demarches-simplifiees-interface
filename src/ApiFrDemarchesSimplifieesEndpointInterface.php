<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-demarches-simplifiees-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDemarchesSimplifiees;

/**
 * ApiFrDemarchesSimplifieesEndpointInterface interface file.
 * 
 * This interface represents an endpoint we have to reach via http(s).
 * 
 * @author Anastaszor
 */
interface ApiFrDemarchesSimplifieesEndpointInterface
{
	
	public const DOSSIER_STATE_INITIATED = 'initiated';
	public const DOSSIER_STATE_CLOSED = 'closed';
	
	/**
	 * Gets the procedure with the given procedure id.
	 *
	 * @param integer $procedureId
	 * @return ApiFrDemarchesSimplifieesProcedureInterface
	 * @throws ApiFrDemarchesSimplifieesThrowable if the query failed or the informations are wrong
	 */
	public function getProcedure(int $procedureId) : ApiFrDemarchesSimplifieesProcedureInterface;
	
	/**
	 * Gets the given page of the procedure with the list of dossiers for this
	 * procedure.
	 *
	 * @param integer $procedureId
	 * @param integer $page
	 * @return ApiFrDemarchesSimplifieesDossierPageInterface
	 * @throws ApiFrDemarchesSimplifieesThrowable if the query failed or the informations are wrong
	 */
	public function getListDossiers(int $procedureId, int $page = 1) : ApiFrDemarchesSimplifieesDossierPageInterface;
	
	/**
	 * Gets the given dossier that belongs to given procedure.
	 *
	 * @param integer $procedureId
	 * @param integer $dossierId
	 * @return ApiFrDemarchesSimplifieesDossierInterface
	 * @throws ApiFrDemarchesSimplifieesThrowable if the query failed or the informations are wrong
	 */
	public function getDossier(int $procedureId, int $dossierId) : ApiFrDemarchesSimplifieesDossierInterface;
	
}
