<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-demarches-simplifiees-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDemarchesSimplifiees;

use Stringable;

/**
 * ApiFrDemarchesSimplifieesGeographicInformationInterface interface file.
 * 
 * This represents some geographic informations for a given entreprise.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiFrDemarchesSimplifieesGeographicInformationInterface extends Stringable
{
	
	/**
	 * Gets whether to use the api carto or not.
	 * 
	 * @return bool
	 */
	public function hasUseApiCarto() : bool;
	
	/**
	 * Gets whether the procedure is in a quartier prioritaire or not.
	 * 
	 * @return bool
	 */
	public function hasQuartiersPrioritaires() : bool;
	
	/**
	 * Gets whether to use the cadastre or not.
	 * 
	 * @return bool
	 */
	public function hasCadastre() : bool;
	
}
