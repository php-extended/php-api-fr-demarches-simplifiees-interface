<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-demarches-simplifiees-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDemarchesSimplifiees;

use DateTimeInterface;
use PhpExtended\Email\EmailAddressInterface;
use Stringable;

/**
 * ApiFrDemarchesSimplifieesUserInterface interface file.
 * 
 * This represents an user.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrDemarchesSimplifieesUserInterface extends Stringable
{
	
	/**
	 * Gets the id of the user.
	 * 
	 * @return int
	 */
	public function getId() : int;
	
	/**
	 * Gets the email of the user.
	 * 
	 * @return EmailAddressInterface
	 */
	public function getEmail() : EmailAddressInterface;
	
	/**
	 * Gets the date of creation of the account.
	 * 
	 * @return DateTimeInterface
	 */
	public function getCreatedAt() : DateTimeInterface;
	
	/**
	 * Gets the date of last update of the account.
	 * 
	 * @return DateTimeInterface
	 */
	public function getUpdatedAt() : DateTimeInterface;
	
	/**
	 * Gets the no siret of the entreprise created.
	 * 
	 * @return string
	 */
	public function getSiret() : string;
	
	/**
	 * Gets whether this user was logged with FC.
	 * 
	 * @return bool
	 */
	public function hasLoggedInWithFranceConnect() : bool;
	
}
