<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-demarches-simplifiees-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDemarchesSimplifiees;

use Stringable;

/**
 * ApiFrDemarchesSimplifieesDossierPageInterface interface file.
 * 
 * This represents a page from the list of dossiers.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiFrDemarchesSimplifieesDossierPageInterface extends Stringable
{
	
	/**
	 * Gets the dossiers items of this list.
	 * 
	 * @return array<int, ApiFrDemarchesSimplifieesDossierItemInterface>
	 */
	public function getDossiers() : array;
	
	/**
	 * Gets this represents a pagination for the dossier list.
	 * 
	 * @return ApiFrDemarchesSimplifieesPaginationInterface
	 */
	public function getPagination() : ApiFrDemarchesSimplifieesPaginationInterface;
	
}
