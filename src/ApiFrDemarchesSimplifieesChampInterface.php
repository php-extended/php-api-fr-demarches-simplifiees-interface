<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-demarches-simplifiees-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDemarchesSimplifiees;

use Stringable;

/**
 * ApiFrDemarchesSimplifieesChampInterface interface file.
 * 
 * This represents a field in a form.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrDemarchesSimplifieesChampInterface extends Stringable
{
	
	/**
	 * Gets the user value of this field.
	 * 
	 * @return string
	 */
	public function getValue() : string;
	
	/**
	 * Gets this represents a type of field.
	 * 
	 * @return ApiFrDemarchesSimplifieesTypeChampInterface
	 */
	public function getTypeDeChamp() : ApiFrDemarchesSimplifieesTypeChampInterface;
	
}
