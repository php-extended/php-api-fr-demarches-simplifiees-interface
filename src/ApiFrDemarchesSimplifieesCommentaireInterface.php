<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-demarches-simplifiees-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDemarchesSimplifiees;

use DateTimeInterface;
use PhpExtended\Email\EmailAddressInterface;
use Stringable;

/**
 * ApiFrDemarchesSimplifieesCommentaireInterface interface file.
 * 
 * This represents a comment.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiFrDemarchesSimplifieesCommentaireInterface extends Stringable
{
	
	/**
	 * Gets the email of the commenter.
	 * 
	 * @return EmailAddressInterface
	 */
	public function getEmail() : EmailAddressInterface;
	
	/**
	 * Gets the full body of the comment, plain html.
	 * 
	 * @return string
	 */
	public function getBody() : string;
	
	/**
	 * Gets the date of creation of the comment.
	 * 
	 * @return DateTimeInterface
	 */
	public function getCreatedAt() : DateTimeInterface;
	
	/**
	 * Gets the attachment of this comment.
	 * 
	 * @return ?string
	 */
	public function getAttachment() : ?string;
	
}
