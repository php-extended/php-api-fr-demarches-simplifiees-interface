<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-demarches-simplifiees-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDemarchesSimplifiees;

use Stringable;

/**
 * ApiFrDemarchesSimplifieesEtablissementInterface interface file.
 * 
 * This represents an establishment for a given enterprise.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiFrDemarchesSimplifieesEtablissementInterface extends Stringable
{
	
	/**
	 * Gets the numero siret of the etablissement.
	 * 
	 * @return string
	 */
	public function getSiret() : string;
	
	/**
	 * Gets whether this etablissement is the siege social.
	 * 
	 * @return bool
	 */
	public function hasSiegeSocial() : bool;
	
	/**
	 * Gets the NAF code.
	 * 
	 * @return string
	 */
	public function getNaf() : string;
	
	/**
	 * Gets the name of the NAF item.
	 * 
	 * @return string
	 */
	public function getLibelleNaf() : string;
	
	/**
	 * Gets the full address, multiline string.
	 * 
	 * @return string
	 */
	public function getAdresse() : string;
	
	/**
	 * Gets the numero of the road.
	 * 
	 * @return ?string
	 */
	public function getNumeroVoie() : ?string;
	
	/**
	 * Gets the type of the road.
	 * 
	 * @return ?string
	 */
	public function getTypevoie() : ?string;
	
	/**
	 * Gets the nom of the road.
	 * 
	 * @return ?string
	 */
	public function getNomVoie() : ?string;
	
	/**
	 * Gets the complement d'adresse.
	 * 
	 * @return ?string
	 */
	public function getComplementAdresse() : ?string;
	
	/**
	 * Gets the code postal of the city.
	 * 
	 * @return string
	 */
	public function getCodePostal() : string;
	
	/**
	 * Gets the name of the city.
	 * 
	 * @return string
	 */
	public function getLocalite() : string;
	
	/**
	 * Gets the code insee of the city.
	 * 
	 * @return int
	 */
	public function getCodeInseeLocalite() : int;
	
}
