<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-demarches-simplifiees-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDemarchesSimplifiees;

use PhpExtended\Email\EmailAddressInterface;
use Stringable;

/**
 * ApiFrDemarchesSimplifieesPersonneInterface interface file.
 * 
 * This represents a people working on the dossier.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiFrDemarchesSimplifieesPersonneInterface extends Stringable
{
	
	/**
	 * Gets the email of the personne.
	 * 
	 * @return EmailAddressInterface
	 */
	public function getEmail() : EmailAddressInterface;
	
}
