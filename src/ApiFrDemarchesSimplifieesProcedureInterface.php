<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-demarches-simplifiees-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDemarchesSimplifiees;

use Psr\Http\Message\UriInterface;
use Stringable;

/**
 * ApiFrDemarchesSimplifieesProcedureInterface interface file.
 * 
 * This represents a procedure.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiFrDemarchesSimplifieesProcedureInterface extends Stringable
{
	
	/**
	 * Gets un nom pour la procédure.
	 * 
	 * @return string
	 */
	public function getLabel() : string;
	
	/**
	 * Gets l'id de la procédure.
	 * 
	 * @return int
	 */
	public function getId() : int;
	
	/**
	 * Gets une description de la procédure (html).
	 * 
	 * @return string
	 */
	public function getDescription() : string;
	
	/**
	 * Gets l'organisation qui a créé la procédure.
	 * 
	 * @return ?string
	 */
	public function getOrganisation() : ?string;
	
	/**
	 * Gets la direction qui a créé la procédure.
	 * 
	 * @return ?string
	 */
	public function getDirection() : ?string;
	
	/**
	 * Gets vrai si la procédure est archivée (i.e. n'est plus utilisable).
	 * 
	 * @return ?bool
	 */
	public function hasArchivedAt() : ?bool;
	
	/**
	 * Gets this represents some geographic informations for a given
	 * entreprise.
	 * 
	 * @return ?ApiFrDemarchesSimplifieesGeographicInformationInterface
	 */
	public function getGeographicInformation() : ?ApiFrDemarchesSimplifieesGeographicInformationInterface;
	
	/**
	 * Gets le nombre total de dossiers pour cette procédure.
	 * 
	 * @return int
	 */
	public function getTotalDossier() : int;
	
	/**
	 * Gets un lien vers l'endroit où on peut remplir la procédure.
	 * 
	 * @return ?UriInterface
	 */
	public function getLink() : ?UriInterface;
	
	/**
	 * Gets l'état de la procédure.
	 * 
	 * @return string
	 */
	public function getState() : string;
	
	/**
	 * Gets les types de champs publics (visibles de celui qui remplit).
	 * 
	 * @return array<int, ApiFrDemarchesSimplifieesTypeChampInterface>
	 */
	public function getTypesDeChamp() : array;
	
	/**
	 * Gets les types de champs privés (invisibles de celui qui remplit).
	 * 
	 * @return array<int, ApiFrDemarchesSimplifieesTypeChampInterface>
	 */
	public function getTypesDeChampPrivate() : array;
	
	/**
	 * Gets les types de pieces justificatives demandées par cette procédure.
	 * 
	 * @return array<int, ApiFrDemarchesSimplifieesTypePieceJustificativeInterface>
	 */
	public function getTypesDePieceJustificative() : array;
	
}
