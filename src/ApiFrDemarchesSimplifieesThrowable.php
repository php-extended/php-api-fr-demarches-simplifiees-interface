<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-demarches-simplifiees-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDemarchesSimplifiees;

use Throwable;

/**
 * ApiFrDemarchesSimplifieesThrowable class file.
 *
 * This exception is the generic exception for in this library.
 *
 * @author Anastaszor
 */
interface ApiFrDemarchesSimplifieesThrowable extends Throwable
{
	
	// codes from 0 to 999 are http codes
	// codes from 1000 to 1999 are curl error codes
	/** @see https://curl.haxx.se/libcurl/c/libcurl-errors.html */
	
	/**
	 * Base code for all curl errors.
	 *
	 * @var integer
	 */
	public const ERROR_CURL_TRANSPORT = 1000;
	
	/**
	 * Code for json_decode failures.
	 *
	 * @var integer
	 */
	public const ERROR_JSON_DECODE = 2000;
	
	/**
	 * Code for \PhpExtended\Json\JsonObject failures.
	 *
	 * @var integer
	 */
	public const ERROR_JSON_BUILDING = 2001;
	
	/**
	 * Code for login procedure failures.
	 *
	 * @var integer
	 */
	public const ERROR_DS_LOGGING = 3000;
	
	/**
	 * Code for calling api methods before being logged.
	 *
	 * @var integer
	 */
	public const ERROR_DS_NOTLOGGED = 3001;
	
	/**
	 * Core for calling api methods that returned an exception.
	 *
	 * @var integer
	 */
	public const ERROR_DS_EXCEPTION = 3002;
	
}
