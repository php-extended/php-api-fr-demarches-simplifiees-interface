<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-demarches-simplifiees-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDemarchesSimplifiees;

use Psr\Http\Message\UriInterface;
use Stringable;

/**
 * ApiFrDemarchesSimplifieesTypePieceJustificativeInterface interface file.
 * 
 * This class represents a type of piece justificative.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiFrDemarchesSimplifieesTypePieceJustificativeInterface extends Stringable
{
	
	/**
	 * Gets the id of the type of piece justificative.
	 * 
	 * @return int
	 */
	public function getId() : int;
	
	/**
	 * Gets the libelle of the type of piece justificative.
	 * 
	 * @return string
	 */
	public function getLibelle() : string;
	
	/**
	 * Gets the description of the type of piece justificative.
	 * 
	 * @return string
	 */
	public function getDescription() : string;
	
	/**
	 * Gets the place of the type of piece justificative. Indexed by 0.
	 * 
	 * @return int
	 */
	public function getOrderPlace() : int;
	
	/**
	 * Gets the lien demarche of the type of piece justificative.
	 * 
	 * @return UriInterface
	 */
	public function getLienDemarche() : UriInterface;
	
}
