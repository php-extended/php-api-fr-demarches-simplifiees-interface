<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-demarches-simplifiees-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDemarchesSimplifiees;

use DateTimeInterface;
use Stringable;

/**
 * ApiFrDemarchesSimplifieesEntrepriseInterface interface file.
 * 
 * This represents an enterprise.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiFrDemarchesSimplifieesEntrepriseInterface extends Stringable
{
	
	/**
	 * Gets the numero siren of the entreprise.
	 * 
	 * @return ?string
	 */
	public function getSiren() : ?string;
	
	/**
	 * Gets the capital social of the entreprise.
	 * 
	 * @return ?string
	 */
	public function getCapitalSocial() : ?string;
	
	/**
	 * Gets the numero tva intracommunautaire.
	 * 
	 * @return ?string
	 */
	public function getNumeroTvaIntracommunautaire() : ?string;
	
	/**
	 * Gets the forme juridique of the entreprise.
	 * 
	 * @return ?string
	 */
	public function getFormeJuridique() : ?string;
	
	/**
	 * Gets the code of forme juridique of the entreprise.
	 * 
	 * @return ?string
	 */
	public function getFormeJuridiqueCode() : ?string;
	
	/**
	 * Gets the nom commercial of the entreprise.
	 * 
	 * @return ?string
	 */
	public function getNomCommercial() : ?string;
	
	/**
	 * Gets the raison sociale of the entreprise.
	 * 
	 * @return string
	 */
	public function getRaisonSociale() : string;
	
	/**
	 * Gets the siret for the siege social.
	 * 
	 * @return ?string
	 */
	public function getSiretSiegeSocial() : ?string;
	
	/**
	 * Gets the code effectif entreprise.
	 * 
	 * @return ?string
	 */
	public function getCodeEffectifEntreprise() : ?string;
	
	/**
	 * Gets the date of creation of the entreprise.
	 * 
	 * @return DateTimeInterface
	 */
	public function getDateCreation() : DateTimeInterface;
	
	/**
	 * Gets the nom of the pdg.
	 * 
	 * @return ?string
	 */
	public function getNom() : ?string;
	
	/**
	 * Gets the prenom of the pdg.
	 * 
	 * @return ?string
	 */
	public function getPrenom() : ?string;
	
}
