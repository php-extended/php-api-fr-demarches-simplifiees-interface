<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-demarches-simplifiees-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDemarchesSimplifiees;

use DateTimeInterface;
use Psr\Http\Message\UriInterface;
use Stringable;

/**
 * ApiFrDemarchesSimplifieesPieceJustificativeInterface interface file.
 * 
 * This represents a piece justificative for the dossier.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiFrDemarchesSimplifieesPieceJustificativeInterface extends Stringable
{
	
	/**
	 * Gets the upload date of the piece justificative.
	 * 
	 * @return DateTimeInterface
	 */
	public function getCreatedAt() : DateTimeInterface;
	
	/**
	 * Gets the id of the related type de piece.
	 * 
	 * @return int
	 */
	public function getTypeDePieceJustificativeId() : int;
	
	/**
	 * Gets the storage url of the piece.
	 * 
	 * @return UriInterface
	 */
	public function getUrl() : UriInterface;
	
	/**
	 * Gets this represents an user.
	 * 
	 * @return ApiFrDemarchesSimplifieesUserInterface
	 */
	public function getUser() : ApiFrDemarchesSimplifieesUserInterface;
	
}
