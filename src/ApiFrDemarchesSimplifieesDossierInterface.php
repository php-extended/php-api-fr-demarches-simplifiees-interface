<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-demarches-simplifiees-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDemarchesSimplifiees;

use DateTimeInterface;
use PhpExtended\Email\EmailAddressInterface;
use Stringable;

/**
 * ApiFrDemarchesSimplifieesDossierInterface interface file.
 * 
 * This represents a dossier for a given procedure.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiFrDemarchesSimplifieesDossierInterface extends Stringable
{
	
	/**
	 * Gets the id of the dossier.
	 * 
	 * @return int
	 */
	public function getId() : int;
	
	/**
	 * Gets the creation date and time.
	 * 
	 * @return DateTimeInterface
	 */
	public function getCreatedAt() : DateTimeInterface;
	
	/**
	 * Gets the last updated date and time.
	 * 
	 * @return DateTimeInterface
	 */
	public function getUpdatedAt() : DateTimeInterface;
	
	/**
	 * Gets the initiated date and time.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getInitiatedAt() : ?DateTimeInterface;
	
	/**
	 * Gets the received date and time.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getReceivedAt() : ?DateTimeInterface;
	
	/**
	 * Gets the received date and time.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getProcessedAt() : ?DateTimeInterface;
	
	/**
	 * Gets whether the dossier is archived.
	 * 
	 * @return ?bool
	 */
	public function hasArchived() : ?bool;
	
	/**
	 * Gets the instructeurs for this dossier.
	 * 
	 * @return array<int, ApiFrDemarchesSimplifieesPersonneInterface>
	 */
	public function getInstructeurs() : array;
	
	/**
	 * Gets the individuals for this dossier.
	 * 
	 * @return array<int, ApiFrDemarchesSimplifieesPersonneInterface>
	 */
	public function getIndividual() : array;
	
	/**
	 * Gets whether the etablissement is a mandataire social.
	 * 
	 * @return ?bool
	 */
	public function hasMandataireSocial() : ?bool;
	
	/**
	 * Gets the motivation of the dossier.
	 * 
	 * @return ?string
	 */
	public function getMotivation() : ?string;
	
	/**
	 * Gets the email of the dossier.
	 * 
	 * @return ?EmailAddressInterface
	 */
	public function getEmail() : ?EmailAddressInterface;
	
	/**
	 * Gets the simplified date of the dossier.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getSimplifiedDate() : ?DateTimeInterface;
	
	/**
	 * Gets the state of the dossier.
	 * 
	 * @return string
	 */
	public function getSimplifiedState() : string;
	
	/**
	 * Gets the state of the dossier.
	 * 
	 * @return string
	 */
	public function getState() : string;
	
	/**
	 * Gets the entreprise source of the dossier.
	 * 
	 * @return ApiFrDemarchesSimplifieesEntrepriseInterface
	 */
	public function getEntreprise() : ApiFrDemarchesSimplifieesEntrepriseInterface;
	
	/**
	 * Gets the etablissement source of the dossier.
	 * 
	 * @return ApiFrDemarchesSimplifieesEtablissementInterface
	 */
	public function getEtablissement() : ApiFrDemarchesSimplifieesEtablissementInterface;
	
	/**
	 * Gets the commentaires of this dossier.
	 * 
	 * @return array<int, ApiFrDemarchesSimplifieesCommentaireInterface>
	 */
	public function getCommentaires() : array;
	
	/**
	 * Gets the champs of the form.
	 * 
	 * @return array<int, ApiFrDemarchesSimplifieesChampInterface>
	 */
	public function getChamps() : array;
	
	/**
	 * Gets the private champs of the form.
	 * 
	 * @return array<int, ApiFrDemarchesSimplifieesChampInterface>
	 */
	public function getChampsPrivate() : array;
	
	/**
	 * Gets the pieces justificatives.
	 * 
	 * @return array<int, ApiFrDemarchesSimplifieesPieceJustificativeInterface>
	 */
	public function getPiecesJustificatives() : array;
	
	/**
	 * Gets the types de pieces justificatives.
	 * 
	 * @return array<int, ApiFrDemarchesSimplifieesTypePieceJustificativeInterface>
	 */
	public function getTypesDePieceJustificatives() : array;
	
}
