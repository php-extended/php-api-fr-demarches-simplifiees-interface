<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-demarches-simplifiees-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrDemarchesSimplifiees;

use Stringable;

/**
 * ApiFrDemarchesSimplifieesPaginationInterface interface file.
 * 
 * This represents a pagination for the dossier list.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiFrDemarchesSimplifieesPaginationInterface extends Stringable
{
	
	/**
	 * Gets the current page number (indexed from 1).
	 * 
	 * @return int
	 */
	public function getPage() : int;
	
	/**
	 * Gets the quantity of results per page.
	 * 
	 * @return int
	 */
	public function getResultatsParPage() : int;
	
	/**
	 * Gets the total number of pages for this query.
	 * 
	 * @return int
	 */
	public function getNombreDePage() : int;
	
}
